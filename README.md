# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository will check the usage of CPU and memory utilization of the system using glances package of python

### How do I get set up? ###

Clone a repository and then from the 'virtual_env' directory activate the virtual environment with 'glances/Scripts/activate'.
After activating the virtual environment, start the glances server in the directory 'Configuration' using the following command:

glances -C glances.conf --disable-plugin docker,wifi
